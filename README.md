# QuickOTP #


A C# implementation of a One-Time Pad I wrote in Python a while ago. This implementation includes a .NET interface with all sorts of widgets and setttings to make encrypting messages with a fixed symbol-set easy and fast.

![qotp.png](https://bitbucket.org/repo/6BEL84/images/6080729-qotp.png)

## Building ##

This project was built in VS2010 C# Express. Install VS2010 and load the .sln .