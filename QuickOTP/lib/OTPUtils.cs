﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuickOTP
{
    public class OTPUtils
    {
        public static string sanitisePlaintext(string plaintext, int keyWidth, EncryptionDictionary d_enc)
        {
            plaintext = OTPUtils.padString(keyWidth, plaintext, d_enc.padchar);
            plaintext = plaintext.Replace(' ', '_');
            return plaintext;
        }


        public static string padString(int width, string text, char padchar)
        {
            int i = width - text.Length;
            for (; i > 0; i--) text += padchar;
            return text;
        }

        public static string genKey(int width, EncryptionDictionary d_enc, int dictSymbolCount)
        {
            string keytext = "";
            Random rng = new Random();

            for (int i = 0; i < width; i++)
            {
                keytext += d_enc.getValueByKey(rng.Next(0, dictSymbolCount - 1));
            }

            return keytext;
        }

        public static string encrypt(string plaintext, string keytext, EncryptionDictionary d_enc)
        {
            string ciphertext = "";

            for (int i = 0; i < plaintext.Length; i++)
            {
                ciphertext += d_enc.getValueByKey(
                    (
                        d_enc.getKeyByValue(plaintext[i]) +
                        d_enc.getKeyByValue(keytext[i])
                    ) % d_enc.Length
                );
            }

            //Console.WriteLine("\n\n{0}", ciphertext);

            return ciphertext;
        }

        public static string decrypt(string ciphertext, string keytext, EncryptionDictionary d_enc)
        {
            string foundtext = "";

            for (int i = 0; i < ciphertext.Length; i++)
            {
                foundtext += d_enc.getValueByKey(
                    (
                        d_enc.getKeyByValue(ciphertext[i]) -
                        d_enc.getKeyByValue(keytext[i]) +
                        d_enc.Length
                    ) % d_enc.Length
                );
            }

            return foundtext;
        }
    }
}
