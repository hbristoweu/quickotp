﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuickOTP
{
    /// <summary>
    /// Encapsulating class for two dictionaries
    /// </summary>
    public class EncryptionDictionary
    {
        private static Dictionary<int, char> d_kv = new Dictionary<int, char>();
        private static Dictionary<char, int> d_vk = new Dictionary<char, int>();

        public int Length = 94;
        public char breakchar = '_';
        public char padchar = 'Z';


        public EncryptionDictionary()
        {
            clearDictionaries();
            genDict();
        }

        public EncryptionDictionary(string symbolList, char breakchar, char padchar)
        {
            clearDictionaries();
            genDictFromSymbolList(symbolList, breakchar, padchar);
        }

        ~EncryptionDictionary()
        {
            clearDictionaries();
        }

        public override string ToString(){
            string s_tmp = "";
            foreach (KeyValuePair<int, char> pair in d_kv) s_tmp += pair.Value;
            return s_tmp;   
        }
        
        public int getKeyByValue(char value)
        {
            return d_vk[value];
        }

        public char getValueByKey(int key)
        {
            return d_kv[key];
        }

        private void genDict()
        {
            /* Length is 94 (0-93)*/
            for (int k = 0, i = 33; i < 127; i++, k++)
            {
                d_kv.Add(k, (char)i);
                d_vk.Add((char)i, k);
            } 
            
            Length = 94;
        }

        private void genDictFromSymbolList(string symbolList, char breakchar, char padchar)
        {
            if (!symbolList.Contains(breakchar)) symbolList += breakchar;
            if (!symbolList.Contains(padchar)) symbolList += padchar;
            int k = 0;
            foreach (char c in symbolList){
                d_kv.Add(k, c);
                d_vk.Add(c, k);
                k++;
            }

            this.Length = k;
        }

        private void clearDictionaries()
        {
            d_kv = new Dictionary<int, char>();
            d_vk = new Dictionary<char, int>();
        }
    }
}
