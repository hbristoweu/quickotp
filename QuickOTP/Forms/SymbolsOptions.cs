﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QuickOTP
{
    public partial class SymbolsOptions : Form
    {
        EncryptionDictionary d_enc;
        public bool dictType94          { get; set; }
        public string symbolList         { get; set; }
        public char symbolPaddingChar   { get; set; }
        public char symbolBreakChar     { get; set; }
             

        public SymbolsOptions(EncryptionDictionary t_d_enc)
        {
            InitializeComponent();
            d_enc = t_d_enc;
        }

        private void SymbolsOptions_Load(object sender, EventArgs e)
        {
            label94Charset.Text = d_enc.ToString();
        }

        private void radioDefault94_CheckedChanged(object sender, EventArgs e)
        {
            if (radioDefault94.Checked)
            {
                radioCustom.Checked = false;
                dictType94 = true;
            }
            else
            {
                radioCustom.Checked = true;
                dictType94 = false;
            }

        }

        private void radioCustom_CheckedChanged(object sender, EventArgs e)
        {
            //xor handled by radioDefault94_CheckedChanged()
        }

        private void buttonAccept_Click(object sender, EventArgs e)
        {
            if (containsMultiple(customSymbolListBox.Text))
            {
                MessageBox.Show("Multiple instances of a symbol detected in the symbol list. ");
            }
            else
            {
                this.symbolList = customSymbolListBox.Text;
                this.dictType94 = radioDefault94.Checked;
                this.symbolPaddingChar = textBoxPaddingChar.Text[0];
                this.symbolBreakChar = textBoxBreakChar.Text[0];

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private bool containsMultiple(string str)
        {
            foreach (char c in str) if (str.Count(f => f == c) > 1) return true;
            return false;
        }
    }

}
