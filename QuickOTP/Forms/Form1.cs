﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

/* Todo 
 *  Move code in methods to dedicated class so that 'Quick Encrypt'
 *  does not duplicate code.
 *
 *  Create some kind of test framework, with emphesis on custom symbolsets
 *  
 */

namespace QuickOTP
{
    public partial class Form1 : Form
    {
        public static EncryptionDictionary d_enc;
        public bool g_dictType94;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dictType94();
            updateSymbolListLabels();
        }

        private void dictTypeCustom(string newSymbolset, char paddingchar, char breakchar)
        {
            d_enc = new EncryptionDictionary(newSymbolset, paddingchar, breakchar);
            g_dictType94 = false;
        }

        private void dictType94()
        {
            d_enc = new EncryptionDictionary();
            g_dictType94 = true;
        }

        private void button_sanitisePlaintext_Click(object sender, EventArgs e)
        {
            if (numericPlaintextSelect.Value < richPlaintextBox.TextLength)
                numericPlaintextSelect.Value = richPlaintextBox.TextLength;
            richPlaintextBox.Text = OTPUtils.sanitisePlaintext(richPlaintextBox.Text, (int)numericPlaintextSelect.Value, d_enc);
        }

        private void button_autoGenKeytext_Click(object sender, EventArgs e)
        {
            if (numericPlaintextSelect.Value < richPlaintextBox.TextLength)
                numericPlaintextSelect.Value = richPlaintextBox.TextLength;
            richPlaintextBox.Text = OTPUtils.sanitisePlaintext(richPlaintextBox.Text, (int)numericPlaintextSelect.Value, d_enc);
            
            if (richPlaintextBox.TextLength < 16)
                MessageBox.Show("Not enough text in plaintext box.");
            else
            {
                numericPlaintextSelect.Value = richPlaintextBox.TextLength;
                numericKeytextSelect.Value = richPlaintextBox.TextLength;
                richKeytextBox.Text = OTPUtils.genKey(richPlaintextBox.TextLength, d_enc, d_enc.Length);
            }
        }

        private void button_fixedGenKeytext_Click(object sender, EventArgs e)
        {
            richKeytextBox.Text = OTPUtils.genKey((int)numericKeytextSelect.Value, d_enc, d_enc.Length);
        }

        private void button_genCiphertext_Click(object sender, EventArgs e)
        {
            // Put this somewhere else
            if (checkPlaintextSymbols())
            {
                if (richPlaintextBox.TextLength < 16 || richKeytextBox.TextLength < 16)
                    MessageBox.Show("Not enough text in plaintext or keytext box.");
                else if (richPlaintextBox.TextLength != richKeytextBox.TextLength)
                    MessageBox.Show("Plaintext and Keytext are of different lengths.");
                else
                    richCiphertextBox.Text = OTPUtils.encrypt(richPlaintextBox.Text, richKeytextBox.Text, d_enc);
            }
            else
            {
                MessageBox.Show("A symbol not in the pad's symbol list was encountered in the plaintext box. The Sanitise button may help.");
            }
        }

        private void button_decrypt_Click(object sender, EventArgs e)
        {
            richFoundtextBox.Text = OTPUtils.decrypt(richCiphertextBox.Text, richKeytextBox.Text, d_enc);
        }

        private void button_quickEncrypt_Click(object sender, EventArgs e)
        {
            // Port all of these methods to a dedicated class
            // so that this function may call all operations without 
            // repeat code.
        }

        private void symbolListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (SymbolsOptions f = new SymbolsOptions(d_enc))
            {
                DialogResult result = f.ShowDialog();
                if (result == DialogResult.OK)
                {
                    if (f.dictType94) dictType94();
                    else dictTypeCustom(f.symbolList, f.symbolPaddingChar, f.symbolBreakChar);
                }
            }

            updateSymbolListLabels();
        }

        private void updateSymbolListLabels()
        {
            labelSymbolListInfo.Text = "Symbol count: " + d_enc.ToString().Length.ToString();
            labelSymbolList.Text = d_enc.ToString();
        }

        private void label_Keytext_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Current symbol list: " + d_enc.ToString());
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (AboutForm a = new AboutForm()) a.ShowDialog();
        }

        private void terminologyToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private bool checkPlaintextSymbols()
        {
            foreach (char c in richPlaintextBox.Text)
                if (!d_enc.ToString().Contains(c))
                    return false;
            return true;
        }
    }


}
