﻿namespace QuickOTP
{
    partial class SymbolsOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioDefault94 = new System.Windows.Forms.RadioButton();
            this.radioCustom = new System.Windows.Forms.RadioButton();
            this.customSymbolListBox = new System.Windows.Forms.TextBox();
            this.label94Charset = new System.Windows.Forms.Label();
            this.buttonAccept = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxPaddingChar = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxBreakChar = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // radioDefault94
            // 
            this.radioDefault94.AutoSize = true;
            this.radioDefault94.Checked = true;
            this.radioDefault94.Location = new System.Drawing.Point(12, 12);
            this.radioDefault94.Name = "radioDefault94";
            this.radioDefault94.Size = new System.Drawing.Size(194, 17);
            this.radioDefault94.TabIndex = 0;
            this.radioDefault94.TabStop = true;
            this.radioDefault94.Text = "Use default 94 character symbol set";
            this.radioDefault94.UseVisualStyleBackColor = true;
            this.radioDefault94.CheckedChanged += new System.EventHandler(this.radioDefault94_CheckedChanged);
            // 
            // radioCustom
            // 
            this.radioCustom.AutoSize = true;
            this.radioCustom.Location = new System.Drawing.Point(12, 62);
            this.radioCustom.Name = "radioCustom";
            this.radioCustom.Size = new System.Drawing.Size(133, 17);
            this.radioCustom.TabIndex = 1;
            this.radioCustom.Text = "Use custom symbol set";
            this.radioCustom.UseVisualStyleBackColor = true;
            this.radioCustom.CheckedChanged += new System.EventHandler(this.radioCustom_CheckedChanged);
            // 
            // customSymbolListBox
            // 
            this.customSymbolListBox.Location = new System.Drawing.Point(11, 85);
            this.customSymbolListBox.Name = "customSymbolListBox";
            this.customSymbolListBox.Size = new System.Drawing.Size(587, 20);
            this.customSymbolListBox.TabIndex = 2;
            // 
            // label94Charset
            // 
            this.label94Charset.AutoSize = true;
            this.label94Charset.Location = new System.Drawing.Point(13, 37);
            this.label94Charset.Name = "label94Charset";
            this.label94Charset.Size = new System.Drawing.Size(35, 13);
            this.label94Charset.TabIndex = 3;
            this.label94Charset.Text = "label1";
            // 
            // buttonAccept
            // 
            this.buttonAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAccept.Location = new System.Drawing.Point(523, 140);
            this.buttonAccept.Name = "buttonAccept";
            this.buttonAccept.Size = new System.Drawing.Size(75, 23);
            this.buttonAccept.TabIndex = 4;
            this.buttonAccept.Text = "Accept";
            this.buttonAccept.UseVisualStyleBackColor = true;
            this.buttonAccept.Click += new System.EventHandler(this.buttonAccept_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Padding Character";
            // 
            // textBoxPaddingChar
            // 
            this.textBoxPaddingChar.Location = new System.Drawing.Point(115, 109);
            this.textBoxPaddingChar.Name = "textBoxPaddingChar";
            this.textBoxPaddingChar.Size = new System.Drawing.Size(30, 20);
            this.textBoxPaddingChar.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(152, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Word break character";
            // 
            // textBoxBreakChar
            // 
            this.textBoxBreakChar.Location = new System.Drawing.Point(269, 109);
            this.textBoxBreakChar.Name = "textBoxBreakChar";
            this.textBoxBreakChar.Size = new System.Drawing.Size(30, 20);
            this.textBoxBreakChar.TabIndex = 8;
            // 
            // SymbolsOptions
            // 
            this.AcceptButton = this.buttonAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(611, 175);
            this.Controls.Add(this.textBoxBreakChar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxPaddingChar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonAccept);
            this.Controls.Add(this.label94Charset);
            this.Controls.Add(this.customSymbolListBox);
            this.Controls.Add(this.radioCustom);
            this.Controls.Add(this.radioDefault94);
            this.Name = "SymbolsOptions";
            this.Text = "Symbol Set Options";
            this.Load += new System.EventHandler(this.SymbolsOptions_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioDefault94;
        private System.Windows.Forms.RadioButton radioCustom;
        private System.Windows.Forms.TextBox customSymbolListBox;
        private System.Windows.Forms.Label label94Charset;
        private System.Windows.Forms.Button buttonAccept;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxPaddingChar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxBreakChar;
    }
}