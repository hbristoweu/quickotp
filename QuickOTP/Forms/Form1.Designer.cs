﻿namespace QuickOTP
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripVersionLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.keytextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.symbolListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.terminologyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.richPlaintextBox = new System.Windows.Forms.RichTextBox();
            this.button_autoGenKeytext = new System.Windows.Forms.Button();
            this.label_Plaintext = new System.Windows.Forms.Label();
            this.richKeytextBox = new System.Windows.Forms.RichTextBox();
            this.label_Keytext = new System.Windows.Forms.Label();
            this.button_fixedGenKeytext = new System.Windows.Forms.Button();
            this.numericKeytextSelect = new System.Windows.Forms.NumericUpDown();
            this.label_Ciphertext = new System.Windows.Forms.Label();
            this.richCiphertextBox = new System.Windows.Forms.RichTextBox();
            this.button_genCiphertext = new System.Windows.Forms.Button();
            this.button_sanitisePlaintext = new System.Windows.Forms.Button();
            this.label_Foundtext = new System.Windows.Forms.Label();
            this.button_quickEncrypt = new System.Windows.Forms.Button();
            this.richFoundtextBox = new System.Windows.Forms.RichTextBox();
            this.numericPlaintextSelect = new System.Windows.Forms.NumericUpDown();
            this.button_decrypt = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.labelSymbolList = new System.Windows.Forms.Label();
            this.labelSymbolListNote = new System.Windows.Forms.Label();
            this.labelSymbolListInfo = new System.Windows.Forms.Label();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericKeytextSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericPlaintextSelect)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripVersionLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 601);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(681, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripVersionLabel
            // 
            this.toolStripVersionLabel.Name = "toolStripVersionLabel";
            this.toolStripVersionLabel.Size = new System.Drawing.Size(44, 17);
            this.toolStripVersionLabel.Text = "V. 0.0.3";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.keytextToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(681, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // keytextToolStripMenuItem
            // 
            this.keytextToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.symbolListToolStripMenuItem});
            this.keytextToolStripMenuItem.Name = "keytextToolStripMenuItem";
            this.keytextToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.keytextToolStripMenuItem.Text = "Symbols";
            // 
            // symbolListToolStripMenuItem
            // 
            this.symbolListToolStripMenuItem.Name = "symbolListToolStripMenuItem";
            this.symbolListToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.symbolListToolStripMenuItem.Text = "Symbol List";
            this.symbolListToolStripMenuItem.Click += new System.EventHandler(this.symbolListToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.terminologyToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // terminologyToolStripMenuItem
            // 
            this.terminologyToolStripMenuItem.Name = "terminologyToolStripMenuItem";
            this.terminologyToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.terminologyToolStripMenuItem.Text = "Terminology";
            this.terminologyToolStripMenuItem.Click += new System.EventHandler(this.terminologyToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // richPlaintextBox
            // 
            this.richPlaintextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.richPlaintextBox.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richPlaintextBox.Location = new System.Drawing.Point(12, 78);
            this.richPlaintextBox.Name = "richPlaintextBox";
            this.richPlaintextBox.Size = new System.Drawing.Size(657, 64);
            this.richPlaintextBox.TabIndex = 2;
            this.richPlaintextBox.Text = "";
            // 
            // button_autoGenKeytext
            // 
            this.button_autoGenKeytext.Location = new System.Drawing.Point(11, 219);
            this.button_autoGenKeytext.Name = "button_autoGenKeytext";
            this.button_autoGenKeytext.Size = new System.Drawing.Size(135, 25);
            this.button_autoGenKeytext.TabIndex = 3;
            this.button_autoGenKeytext.Text = "Generate Keytext (Auto)";
            this.button_autoGenKeytext.UseVisualStyleBackColor = true;
            this.button_autoGenKeytext.Click += new System.EventHandler(this.button_autoGenKeytext_Click);
            // 
            // label_Plaintext
            // 
            this.label_Plaintext.AutoSize = true;
            this.label_Plaintext.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Plaintext.Location = new System.Drawing.Point(12, 24);
            this.label_Plaintext.Name = "label_Plaintext";
            this.label_Plaintext.Size = new System.Drawing.Size(69, 20);
            this.label_Plaintext.TabIndex = 4;
            this.label_Plaintext.Text = "Plaintext";
            // 
            // richKeytextBox
            // 
            this.richKeytextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.richKeytextBox.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richKeytextBox.Location = new System.Drawing.Point(11, 280);
            this.richKeytextBox.Name = "richKeytextBox";
            this.richKeytextBox.Size = new System.Drawing.Size(657, 64);
            this.richKeytextBox.TabIndex = 5;
            this.richKeytextBox.Text = "";
            // 
            // label_Keytext
            // 
            this.label_Keytext.AutoSize = true;
            this.label_Keytext.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Keytext.Location = new System.Drawing.Point(12, 145);
            this.label_Keytext.Name = "label_Keytext";
            this.label_Keytext.Size = new System.Drawing.Size(61, 20);
            this.label_Keytext.TabIndex = 6;
            this.label_Keytext.Text = "Keytext";
            this.toolTip1.SetToolTip(this.label_Keytext, "toolTip1");
            this.label_Keytext.Click += new System.EventHandler(this.label_Keytext_Click);
            // 
            // button_fixedGenKeytext
            // 
            this.button_fixedGenKeytext.Location = new System.Drawing.Point(11, 250);
            this.button_fixedGenKeytext.Name = "button_fixedGenKeytext";
            this.button_fixedGenKeytext.Size = new System.Drawing.Size(135, 25);
            this.button_fixedGenKeytext.TabIndex = 7;
            this.button_fixedGenKeytext.Text = "Generate Keytext";
            this.button_fixedGenKeytext.UseVisualStyleBackColor = true;
            this.button_fixedGenKeytext.Click += new System.EventHandler(this.button_fixedGenKeytext_Click);
            // 
            // numericKeytextSelect
            // 
            this.numericKeytextSelect.Location = new System.Drawing.Point(151, 254);
            this.numericKeytextSelect.Maximum = new decimal(new int[] {
            512,
            0,
            0,
            0});
            this.numericKeytextSelect.Minimum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.numericKeytextSelect.Name = "numericKeytextSelect";
            this.numericKeytextSelect.Size = new System.Drawing.Size(45, 20);
            this.numericKeytextSelect.TabIndex = 8;
            this.numericKeytextSelect.Value = new decimal(new int[] {
            16,
            0,
            0,
            0});
            // 
            // label_Ciphertext
            // 
            this.label_Ciphertext.AutoSize = true;
            this.label_Ciphertext.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Ciphertext.Location = new System.Drawing.Point(11, 347);
            this.label_Ciphertext.Name = "label_Ciphertext";
            this.label_Ciphertext.Size = new System.Drawing.Size(81, 20);
            this.label_Ciphertext.TabIndex = 9;
            this.label_Ciphertext.Text = "Ciphertext";
            // 
            // richCiphertextBox
            // 
            this.richCiphertextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.richCiphertextBox.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richCiphertextBox.Location = new System.Drawing.Point(11, 401);
            this.richCiphertextBox.Name = "richCiphertextBox";
            this.richCiphertextBox.Size = new System.Drawing.Size(655, 64);
            this.richCiphertextBox.TabIndex = 10;
            this.richCiphertextBox.Text = "";
            // 
            // button_genCiphertext
            // 
            this.button_genCiphertext.Location = new System.Drawing.Point(11, 370);
            this.button_genCiphertext.Name = "button_genCiphertext";
            this.button_genCiphertext.Size = new System.Drawing.Size(135, 25);
            this.button_genCiphertext.TabIndex = 11;
            this.button_genCiphertext.Text = "Encrypt Ciphertext";
            this.button_genCiphertext.UseVisualStyleBackColor = true;
            this.button_genCiphertext.Click += new System.EventHandler(this.button_genCiphertext_Click);
            // 
            // button_sanitisePlaintext
            // 
            this.button_sanitisePlaintext.Location = new System.Drawing.Point(12, 47);
            this.button_sanitisePlaintext.Name = "button_sanitisePlaintext";
            this.button_sanitisePlaintext.Size = new System.Drawing.Size(134, 25);
            this.button_sanitisePlaintext.TabIndex = 12;
            this.button_sanitisePlaintext.Text = "Sanitise Given Plaintext";
            this.button_sanitisePlaintext.UseVisualStyleBackColor = true;
            this.button_sanitisePlaintext.Click += new System.EventHandler(this.button_sanitisePlaintext_Click);
            // 
            // label_Foundtext
            // 
            this.label_Foundtext.AutoSize = true;
            this.label_Foundtext.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Foundtext.Location = new System.Drawing.Point(11, 468);
            this.label_Foundtext.Name = "label_Foundtext";
            this.label_Foundtext.Size = new System.Drawing.Size(81, 20);
            this.label_Foundtext.TabIndex = 13;
            this.label_Foundtext.Text = "Foundtext";
            // 
            // button_quickEncrypt
            // 
            this.button_quickEncrypt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.button_quickEncrypt.Location = new System.Drawing.Point(535, 46);
            this.button_quickEncrypt.Name = "button_quickEncrypt";
            this.button_quickEncrypt.Size = new System.Drawing.Size(134, 25);
            this.button_quickEncrypt.TabIndex = 14;
            this.button_quickEncrypt.Text = "Quick Encrypt";
            this.button_quickEncrypt.UseVisualStyleBackColor = true;
            this.button_quickEncrypt.Click += new System.EventHandler(this.button_quickEncrypt_Click);
            // 
            // richFoundtextBox
            // 
            this.richFoundtextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.richFoundtextBox.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richFoundtextBox.Location = new System.Drawing.Point(11, 522);
            this.richFoundtextBox.Name = "richFoundtextBox";
            this.richFoundtextBox.ReadOnly = true;
            this.richFoundtextBox.Size = new System.Drawing.Size(654, 64);
            this.richFoundtextBox.TabIndex = 15;
            this.richFoundtextBox.Text = "";
            // 
            // numericPlaintextSelect
            // 
            this.numericPlaintextSelect.Location = new System.Drawing.Point(152, 51);
            this.numericPlaintextSelect.Maximum = new decimal(new int[] {
            512,
            0,
            0,
            0});
            this.numericPlaintextSelect.Minimum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.numericPlaintextSelect.Name = "numericPlaintextSelect";
            this.numericPlaintextSelect.Size = new System.Drawing.Size(46, 20);
            this.numericPlaintextSelect.TabIndex = 16;
            this.numericPlaintextSelect.Value = new decimal(new int[] {
            16,
            0,
            0,
            0});
            // 
            // button_decrypt
            // 
            this.button_decrypt.Location = new System.Drawing.Point(11, 491);
            this.button_decrypt.Name = "button_decrypt";
            this.button_decrypt.Size = new System.Drawing.Size(134, 25);
            this.button_decrypt.TabIndex = 17;
            this.button_decrypt.Text = "Decrypt Ciphertext";
            this.button_decrypt.UseVisualStyleBackColor = true;
            this.button_decrypt.Click += new System.EventHandler(this.button_decrypt_Click);
            // 
            // labelSymbolList
            // 
            this.labelSymbolList.AutoSize = true;
            this.labelSymbolList.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSymbolList.Location = new System.Drawing.Point(12, 192);
            this.labelSymbolList.Name = "labelSymbolList";
            this.labelSymbolList.Size = new System.Drawing.Size(139, 13);
            this.labelSymbolList.TabIndex = 18;
            this.labelSymbolList.Text = "----------------------";
            // 
            // labelSymbolListNote
            // 
            this.labelSymbolListNote.AutoSize = true;
            this.labelSymbolListNote.Location = new System.Drawing.Point(13, 169);
            this.labelSymbolListNote.Name = "labelSymbolListNote";
            this.labelSymbolListNote.Size = new System.Drawing.Size(97, 13);
            this.labelSymbolListNote.TabIndex = 19;
            this.labelSymbolListNote.Text = "Current Symbol List";
            // 
            // labelSymbolListInfo
            // 
            this.labelSymbolListInfo.AutoSize = true;
            this.labelSymbolListInfo.Location = new System.Drawing.Point(117, 169);
            this.labelSymbolListInfo.Name = "labelSymbolListInfo";
            this.labelSymbolListInfo.Size = new System.Drawing.Size(35, 13);
            this.labelSymbolListInfo.TabIndex = 20;
            this.labelSymbolListInfo.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(681, 623);
            this.Controls.Add(this.labelSymbolListInfo);
            this.Controls.Add(this.labelSymbolListNote);
            this.Controls.Add(this.labelSymbolList);
            this.Controls.Add(this.button_decrypt);
            this.Controls.Add(this.numericPlaintextSelect);
            this.Controls.Add(this.richFoundtextBox);
            this.Controls.Add(this.button_quickEncrypt);
            this.Controls.Add(this.label_Foundtext);
            this.Controls.Add(this.button_sanitisePlaintext);
            this.Controls.Add(this.button_genCiphertext);
            this.Controls.Add(this.richCiphertextBox);
            this.Controls.Add(this.label_Ciphertext);
            this.Controls.Add(this.numericKeytextSelect);
            this.Controls.Add(this.button_fixedGenKeytext);
            this.Controls.Add(this.label_Keytext);
            this.Controls.Add(this.richKeytextBox);
            this.Controls.Add(this.label_Plaintext);
            this.Controls.Add(this.button_autoGenKeytext);
            this.Controls.Add(this.richPlaintextBox);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "QuickOTP";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericKeytextSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericPlaintextSelect)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.RichTextBox richPlaintextBox;
        private System.Windows.Forms.Button button_autoGenKeytext;
        private System.Windows.Forms.Label label_Plaintext;
        private System.Windows.Forms.RichTextBox richKeytextBox;
        private System.Windows.Forms.Label label_Keytext;
        private System.Windows.Forms.ToolStripMenuItem keytextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem symbolListToolStripMenuItem;
        private System.Windows.Forms.Button button_fixedGenKeytext;
        private System.Windows.Forms.NumericUpDown numericKeytextSelect;
        private System.Windows.Forms.Label label_Ciphertext;
        private System.Windows.Forms.RichTextBox richCiphertextBox;
        private System.Windows.Forms.Button button_genCiphertext;
        private System.Windows.Forms.Button button_sanitisePlaintext;
        private System.Windows.Forms.ToolStripStatusLabel toolStripVersionLabel;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem terminologyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Label label_Foundtext;
        private System.Windows.Forms.Button button_quickEncrypt;
        private System.Windows.Forms.RichTextBox richFoundtextBox;
        private System.Windows.Forms.NumericUpDown numericPlaintextSelect;
        private System.Windows.Forms.Button button_decrypt;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Label labelSymbolList;
        private System.Windows.Forms.Label labelSymbolListNote;
        private System.Windows.Forms.Label labelSymbolListInfo;
    }
}

